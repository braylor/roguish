#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger and Brandon Phillips
# This code is released under the license described in the LICENSE file

from setuptools import setup
from pathlib import Path

from roguish._meta import data

readme_path = Path(__file__).absolute().parent / 'README.md'

with readme_path.open() as file:
    long_description = file.read()

setup(
    long_description=long_description,
    long_description_content_type='text/markdown',
    **data
)
