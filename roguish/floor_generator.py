#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2020 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from roguish import Wall, Floor, Cell, UpStairs, DownStairs
from random import Random
from typing import Set, List, Optional, Dict, Tuple


class Room:
    def __init__(self, x: int, y: int, width: int, height: int) -> None:
        self.__x = x
        self.__y = y
        self.__width = width
        self.__height = height

    @property
    def x(self) -> int:
        return self.__x

    @property
    def y(self) -> int:
        return self.__y

    @property
    def width(self) -> int:
        return self.__width

    @property
    def height(self) -> int:
        return self.__height

    def __hash__(self) -> int:
        return hash((
            self.__x,
            self.__y,
            self.__width,
            self.__height,
        ))

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Room):
            return False

        return (
            self.__x == other.__x
            and self.__y == other.__y
            and self.__width == other.__width
            and self.__height == other.__height
        )

    def overlaps(self, other: 'Room') -> bool:
        '''Get whether this and the other room overlap.
        '''

        return not (
            other.__x >= self.__x + self.__width
            or self.__x >= other.__x + other.__width
            or other.__y >= self.__y + self.__height
            or self.__y >= other.__y + other.__height
        )


class Region:
    '''Simple region marker, used to allow proper type checking.
    Only really used as a hashed object for dictionaries.'''

    def __hash__(self) -> int:
        return hash(id(self))

    def __eq__(self, other: object) -> bool:
        return id(self) == id(other)

    def __ne__(self, other: object) -> bool:
        return id(self) != id(other)

    def __lt__(self, other: object) -> bool:
        if not isinstance(other, Region):
            return NotImplemented
        return id(self) < id(other)

    def __le__(self, other: object) -> bool:
        if not isinstance(other, Region):
            return NotImplemented
        return id(self) <= id(other)

    def __gt__(self, other: object) -> bool:
        if not isinstance(other, Region):
            return NotImplemented
        return id(self) > id(other)

    def __ge__(self, other: object) -> bool:
        if not isinstance(other, Region):
            return NotImplemented
        return id(self) >= id(other)


def generate_rooms(max_room_size: int, floor_size: int, random: Random) -> Set[Room]:
    '''Generate a set of non-overlapping rooms of the proper size and level.
    '''
    rooms: Set[Room] = set()

    for _ in range(50):
        width = random.choice(range(3, max_room_size + 1, 2))
        height = random.choice(range(3, max_room_size + 1, 2))

        # Get the position as an odd number that doesn't overlap the edge of
        # the floor.
        x = random.randint(0, (floor_size - width) // 2 - 1) * 2 + 1
        y_offset = random.randint(0, (floor_size - height) // 2 - 1) * 2 + 1
        room = Room(
            x=x,
            y=y_offset,
            width=width,
            height=height,
        )

        # Add the room if it doesn't overlap other rooms.
        if not any(room.overlaps(other) for other in rooms):
            rooms.add(room)
    return rooms


def dig_rooms(
    rooms: Set[Room],
    floor: List[List[Cell]],
    regions: Set[Region],
    region_tiles: Dict[Tuple[int, int], Region],
):
    '''Dig the rooms out of the floor and add them to the region set.
    '''
    for room in rooms:
        region = Region()
        regions.add(region)
        for y_offset in range(room.height):
            for x_offset in range(room.width):
                x = room.x + x_offset
                y = room.y + y_offset
                tile = Floor(x, y)
                floor[y][x] = tile
                region_tiles[(x, y)] = region


def dig_mazes(
    floor: List[List[Cell]],
    regions: Set[Region],
    region_tiles: Dict[Tuple[int, int], Region],
    random: Random,
):
    '''Dig out all the mazes, properly regioning each of them.
    '''
    size = len(floor)
    for y in range(1, size, 2):
        for x in range(1, size, 2):
            start = floor[y][x]
            if isinstance(start, Wall):
                region = Region()
                regions.add(region)
                start = Floor(x, y)
                region_tiles[(x, y)] = region
                floor[y][x] = start
                stack: List[Floor] = [start]
                while stack:
                    current = stack.pop()
                    neighbors: List[Wall] = []
                    if current.x > 1:
                        neighbor = floor[current.y][current.x - 2]
                        if isinstance(neighbor, Wall):
                            neighbors.append(neighbor)

                    if current.x + 2 < size:
                        neighbor = floor[current.y][current.x + 2]
                        if isinstance(neighbor, Wall):
                            neighbors.append(neighbor)

                    if current.y > 1:
                        neighbor = floor[current.y - 2][current.x]
                        if isinstance(neighbor, Wall):
                            neighbors.append(neighbor)

                    if current.y + 2 < size:
                        neighbor = floor[current.y + 2][current.x]
                        if isinstance(neighbor, Wall):
                            neighbors.append(neighbor)

                    if neighbors:
                        stack.append(current)
                        neighbor = random.choice(neighbors)
                        neighbor = Floor(neighbor.x, neighbor.y)
                        region_tiles[(neighbor.x, neighbor.y)] = region
                        floor[neighbor.y][neighbor.x] = neighbor
                        stack.append(neighbor)

                        divider_x = (current.x + neighbor.x) // 2
                        divider_y = (current.y + neighbor.y) // 2

                        divider = Floor(divider_x, divider_y)
                        floor[divider_y][divider_x] = divider
                        region_tiles[(divider.x, divider.y)] = region


def get_separators(
    floor: List[List[Cell]],
    region_tiles: Dict[Tuple[int, int], Region],
) -> Dict[Tuple[Region, Region], Set[Tuple[int, int]]]:
    '''Get a dictionary of separators between each pair of regions that is
    adjacent.
    '''
    size = len(floor)
    separators: Dict[Tuple[Region, Region], Set[Tuple[int, int]]] = {}
    for y, row in enumerate(floor):
        for x, cell in enumerate(row):
            neighbors: Set[Cell] = set()
            neighbor_regions: Set[Region] = set()
            if x > 0:
                neighbors.add(row[x - 1])
            if x + 1 < size:
                neighbors.add(row[x + 1])
            if y > 0:
                neighbors.add(floor[y - 1][x])
            if y + 1 < size:
                neighbors.add(floor[y + 1][x])

            for neighbor in neighbors:
                if (neighbor.x, neighbor.y) in region_tiles:
                    neighbor_regions.add(region_tiles[neighbor.x, neighbor.y])
            for first in neighbor_regions:
                for second in neighbor_regions:
                    if first is not second:
                        if first < second:
                            pair = first, second
                        else:
                            pair = second, first
                        if pair not in separators:
                            separators[pair] = set()
                        separators[pair].add((cell.x, cell.y))
    return separators


def join_regions(
    floor: List[List[Cell]],
    separators: Dict[Tuple[Region, Region], Set[Tuple[int, int]]],
    random: Random,
):
    '''Join regions by deleting a random wall until all regions are one.
    '''
    while separators:
        (separated_regions, walls) = random.choice(list(separators.items()))
        wall = random.choice(list(walls))
        walls.remove(wall)
        floor[wall[1]][wall[0]] = Floor(wall[0], wall[1])

        del separators[separated_regions]

        # Remove the lower-value region
        keep_region = separated_regions[0]
        remove_region = separated_regions[1]

        # Replace all regions with the remove region with the new region
        for key, walls in list(separators.items()):
            if remove_region in key:
                alpha, beta = key
                if remove_region is alpha:
                    alpha = keep_region
                else:
                    beta = keep_region

                # We don't have to consider the case where they are equal,
                # because we've already deleted the separated regions.
                if alpha < beta:
                    new_key = alpha, beta
                elif alpha > beta:
                    new_key = beta, alpha

                separators[new_key] = separators.pop(key)


def backtrack_deadends(
    floor: List[List[Cell]],
    max_deadends: int,
):
    '''Repeatedly backtrack all dead ends until we are within the acceptable max range.
    '''

    size = len(floor)

    while True:
        deadends: Set[Floor] = set()

        for y, row in enumerate(floor):
            for x, cell in enumerate(row):
                if isinstance(cell, Floor):
                    wall_neighbors = 0
                    if x > 0:
                        neighbor = row[x - 1]
                        if isinstance(neighbor, Wall):
                            wall_neighbors += 1
                    if x + 1 < size:
                        neighbor = row[x + 1]
                        if isinstance(neighbor, Wall):
                            wall_neighbors += 1
                    if y > 0:
                        neighbor = floor[y - 1][x]
                        if isinstance(neighbor, Wall):
                            wall_neighbors += 1
                    if y + 1 < size:
                        neighbor = floor[y + 1][x]
                        if isinstance(neighbor, Wall):
                            wall_neighbors += 1

                    if wall_neighbors >= 3:
                        deadends.add(cell)
        if len(deadends) > max_deadends:
            for deadend in deadends:
                floor[deadend.y][deadend.x] = Wall(deadend.x, deadend.y)
        else:
            break


def generate(level: int, seed: Optional[int] = None) -> List[List[Cell]]:
    '''Simple floor generator.
    Level goes from 0, incrementing as the player goes down.
    '''

    if seed is None:
        random = Random()
    else:
        random = Random(seed)

    size = level * 2 + 21

    # First we fill the floor with walls
    floor: List[List[Cell]] = [[Wall(x, y) for x in range(size)] for y in range(size)]

    # A set of regions, used for later reduction
    regions: Set[Region] = set()

    # A mapping from each tile to their regions
    region_tiles: Dict[Tuple[int, int], Region] = {}

    # Then we randomly generate a list of potential rooms.  We shouldn't need
    # to use a list (to avoid favoring particular combinations which will get
    # steady hashes), because sets maintain order as of cpython 3.7.
    rooms = generate_rooms(
        max_room_size=5 + level // 3,
        random=random,
        floor_size=size,
    )

    # Actually dig out the rooms
    dig_rooms(
        rooms=rooms,
        floor=floor,
        regions=regions,
        region_tiles=region_tiles,
    )

    room_iter = iter(rooms)
    upstairs_room = next(room_iter)

    x = random.randrange(upstairs_room.width) + upstairs_room.x
    y = random.randrange(upstairs_room.height) + upstairs_room.y
    floor[y][x] = UpStairs(x, y)

    downstairs_room = next(room_iter)
    x = random.randrange(downstairs_room.width) + downstairs_room.x
    y = random.randrange(downstairs_room.height) + downstairs_room.y
    floor[y][x] = DownStairs(x, y)

    dig_mazes(
        floor=floor,
        regions=regions,
        region_tiles=region_tiles,
        random=random,
    )

    separators = get_separators(
        floor=floor,
        region_tiles=region_tiles,
    )

    join_regions(
        floor=floor,
        random=random,
        separators=separators,
    )

    backtrack_deadends(
        floor=floor,
        max_deadends=random.randint(0, level),
    )

    # Set the cell neighborhood
    for y, row in enumerate(floor):
        for x, cell in enumerate(row):
            if x > 0:
                cell.west = row[x - 1]
            if x + 1 < size:
                cell.east = row[x + 1]
            if y > 0:
                cell.north = floor[y - 1][x]
            if y + 1 < size:
                cell.south = floor[y + 1][x]

    return floor
