from typing import List, Tuple, Optional, Iterator
import pygame
from pygame.transform import smoothscale
from pygame.font import Font
from pygame import Color, Rect, Surface
import weakref
from enum import Enum, unique, auto
from typing import NamedTuple


class GlyphMetric(NamedTuple):
    '''A single named glyph metric for more readable rendering.'''
    xmin: int
    xmax: int
    ymin: int
    ymax: int
    advance: int


class Exit(Exception):
    pass


@unique
class RenderMode(Enum):
    #: Center the rendering in the tile.  Overflow will be clipped.
    CENTER = auto()

    #: Stretch to fill the entire tile.  Downstretching will also happen if
    #: necessary.
    FILL = auto()

    #: Stretch to fill the inner 80% of the tile.  Downstretching will also
    #: happen if necessary.
    MARGIN = auto()


class Renderable:
    '''Anything that can be rendered, including all cells and cell contents.

    Any children of Renderable, no matter how distant, **must** call
    super().__init__().
    '''

    foreground_color = Color(255, 255, 255, 255)
    background_color = Color(0, 0, 0, 0)

    def __init__(self, render_mode: RenderMode = RenderMode.CENTER):
        assert type(self) is not Renderable
        self.__surface: Optional[Surface] = None
        self.__render_mode = render_mode
        self.__rendered_size: Optional[int] = None

    @property
    def icon(self) -> str:
        raise NotImplementedError()

    @property
    def render_mode(self) -> RenderMode:
        return self.__render_mode

    def render(self, font: Font, size: int) -> Surface:
        '''Render and cache the current Renderable.'''

        if self.__surface is None or self.__rendered_size != size:
            surface = font.render(self.icon, True, self.foreground_color)

            ascent = font.get_ascent()
            top = surface.get_height()
            bottom = 0
            left = surface.get_width()
            right = 0
            advance = 0

            for metrics in font.metrics(self.icon):
                metric = GlyphMetric(*metrics)
                left = min(left, metric.xmin + advance)
                right = max(right, metric.xmax + advance)
                advance += metric.advance
                top = min(top, ascent - metric.ymax)
                bottom = max(bottom, ascent - metric.ymin)

            # special case for blank space
            if left == right and top == bottom:
                self.__surface = Surface((size, size), flags=pygame.SRCALPHA)
                self.__surface.fill(self.background_color)
                self.__rendered_size = size
                return self.__surface

            if left >= right:
                raise RuntimeError(f'Bad font render: left({left}) >= right({right})')

            if top >= bottom:
                raise RuntimeError(f'Bad font render: top({top}) >= bottom({bottom})')

            surface = surface.subsurface(Rect(left, top, right - left, bottom - top))

            self.__surface = Surface((size, size), flags=pygame.SRCALPHA)
            self.__surface.fill(self.background_color)

            if self.render_mode is RenderMode.CENTER:
                x = size // 2 - surface.get_width() // 2
                y = size // 2 - surface.get_height() // 2
                self.__surface.blit(surface, (x, y))
            elif self.render_mode is RenderMode.FILL:
                self.__surface.blit(smoothscale(surface, (size, size)), (0, 0))
            elif self.render_mode is RenderMode.MARGIN:
                width = int(size * 0.8)
                offset = int(size * 0.1)
                self.__surface.blit(
                    smoothscale(surface, (width, width)),
                    (offset, offset),
                )
            else:
                raise RuntimeError(f'Unknown RenderMode: {self.__render_mode}')

            self.__rendered_size = size

        return self.__surface


class CellContent(Renderable):
    '''Anything that can be contained in a cell.  Includes creatures like
    players and enemies and also items and doors.
    '''


class Creature(CellContent):
    '''Type of everything with HP.'''

    def __init__(self, max_hp: int, current_hp: Optional[int] = None) -> None:
        assert type(self) is not Creature
        super().__init__(render_mode=RenderMode.MARGIN)
        self.__max_hp = max_hp
        self.__hp = max_hp

    @property
    def max_hp(self) -> int:
        return self.__max_hp

    @property
    def hp(self) -> int:
        return self.__hp

    def take_damage(self, damage: int) -> None:
        self.__hp -= damage

    def dead(self) -> bool:
        return self.__hp <= 0


class Player(Creature):
    '''The core player class, containing the player's hp, items, etc.
    '''
    icon = '@'

    def __init__(self) -> None:
        super().__init__(max_hp=10)

        self.__exp = 0
        self.__cell: Optional[weakref.ReferenceType[Cell]] = None

    @property
    def position(self) -> Tuple[int, int]:
        cell = self.cell
        if cell is None:
            raise RuntimeError('Player is not in a cell!')
        return cell.position

    @property
    def cell(self) -> Optional['Cell']:
        if self.__cell:
            return self.__cell()
        return None

    @cell.setter
    def cell(self, value: 'Cell') -> None:
        if value is None:
            self.__cell = None
        else:
            self.__cell = weakref.ref(value)

    def try_move_east(self) -> None:
        cell = self.cell
        if cell is None:
            return
        east = cell.east
        if east is None or isinstance(east, Wall):
            return
        # TODO: also check for enemy
        east.add(self)

    def try_move_west(self) -> None:
        cell = self.cell
        if cell is None:
            return
        west = cell.west
        if west is None or isinstance(west, Wall):
            return
        west.add(self)

    def try_move_north(self) -> None:
        cell = self.cell
        if cell is None:
            return
        north = cell.north
        if north is None or isinstance(north, Wall):
            return
        north.add(self)

    def try_move_south(self) -> None:
        cell = self.cell
        if cell is None:
            return
        south = cell.south
        if south is None or isinstance(south, Wall):
            return
        south.add(self)


class Cell(Renderable):
    '''Base cell type for every level.

    This shouldn't be directly instantiated, only subclassed, and the subclass
    needs to be sure to always call self().__init__(...).
    '''

    def __init__(self, x: int, y: int) -> None:
        assert type(self) is not Cell
        super().__init__(render_mode=RenderMode.FILL)

        self.__x = x
        self.__y = y
        self.__contents: List[CellContent] = []
        self.__west: Optional[weakref.ReferenceType[Cell]] = None
        self.__east: Optional[weakref.ReferenceType[Cell]] = None
        self.__north: Optional[weakref.ReferenceType[Cell]] = None
        self.__south: Optional[weakref.ReferenceType[Cell]] = None
        self.__surface: Optional[Surface] = None
        self.__rendered_size: Optional[int] = None

    def __repr__(self) -> str:
        return f'<{type(self).__name__} ({self.__x}, {self.__y})>'

    @property
    def position(self) -> Tuple[int, int]:
        return (self.__x, self.__y)

    @property
    def x(self) -> int:
        return self.__x

    @property
    def y(self) -> int:
        return self.__y

    @property
    def west(self) -> Optional['Cell']:
        if self.__west:
            return self.__west()
        return None

    @west.setter
    def west(self, value: 'Cell'):
        self.__west = weakref.ref(value)

    @property
    def east(self) -> Optional['Cell']:
        if self.__east:
            return self.__east()
        return None

    @east.setter
    def east(self, value: 'Cell'):
        self.__east = weakref.ref(value)

    @property
    def north(self) -> Optional['Cell']:
        if self.__north:
            return self.__north()
        return None

    @north.setter
    def north(self, value: 'Cell'):
        self.__north = weakref.ref(value)

    @property
    def south(self) -> Optional['Cell']:
        if self.__south:
            return self.__south()
        return None

    @south.setter
    def south(self, value: 'Cell'):
        self.__south = weakref.ref(value)

    @property
    def contents(self) -> Iterator[CellContent]:
        '''Get a read-only iterator of contents.'''

        return iter(self.__contents)

    def add(self, value: CellContent) -> None:
        '''Add something to this cell's contents.'''
        if value not in self.__contents:
            self.__contents.append(value)
            if isinstance(value, Player):
                player: Player = value
                if player.cell is not None:
                    player.cell.remove(player)
                player.cell = self

        self.__surface = None

    def remove(self, value: CellContent) -> None:
        '''Remove something from this cell's contents.'''
        self.__contents.remove(value)
        if isinstance(value, Player):
            value.cell = None
        self.__surface = None

    def render(self, font: Font, size: int) -> Surface:
        '''Render and cache the current Renderable'''
        if self.__surface is None or self.__rendered_size != size:
            # We copy it so that we don't have to rerender the supercell or
            # anything like that.
            surface = super().render(font=font, size=size).copy()
            for content in self.__contents:
                surface.blit(content.render(font=font, size=size), dest=(0, 0))

            self.__surface = surface
            self.__rendered_size = size
        return self.__surface


class Floor(Cell):
    icon = ' '


class Wall(Cell):
    icon = ' '
    background_color = Color(255, 255, 255, 255)


class UpStairs(Cell):
    icon = '↑'
    render_mode = RenderMode.CENTER


class DownStairs(Cell):
    icon = '↓'
    render_mode = RenderMode.CENTER
