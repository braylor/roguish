from contextlib import closing
from pygame import Color
from pygame.font import Font
from roguish import Exit, Cell, Wall, UpStairs, DownStairs, Floor, Player
from roguish.floor_generator import generate
from tempfile import NamedTemporaryFile
from textwrap import dedent
from typing import List, Tuple
from time import sleep
import pkg_resources
import pygame
import shutil
from datetime import datetime


class Dungeon:
    def __init__(self) -> None:
        self.__floors: List[List[List[Cell]]] = []

    def __getitem__(self, key: int) -> List[List[Cell]]:
        '''Get a floor, generating it if necessary'''
        if key < 0:
            raise IndexError('negative keys are not allowed')

        while len(self.__floors) <= key:
            level = len(self.__floors)
            self.__floors.append(generate(level))

        return self.__floors[key]


class Engine:
    def __init__(self) -> None:
        self.dungeon = Dungeon()
        self.level = 0
        self.floor = self.dungeon[0]

        self.player = Player()

        for row in self.floor:
            for cell in row:
                if isinstance(cell, UpStairs):
                    cell.add(self.player)
                    break

        self.cell_size = 48

        # This must be called before any other pygame code
        pygame.init()
        self.font_file = NamedTemporaryFile('wb', suffix='.txt')
        with closing(pkg_resources.resource_stream('roguish', 'fonts/NotoSansMono-Regular.ttf')) as font:
            shutil.copyfileobj(font, self.font_file)
        self.font_file.flush()

        self.font = Font(self.font_file.name, self.cell_size)

    def handle_events(self) -> None:
        '''Loop through events and handle them'''
        for event in [pygame.event.wait()] + pygame.event.get():
            if event.type == pygame.QUIT:
                raise Exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.player.try_move_west()
                elif event.key == pygame.K_RIGHT:
                    self.player.try_move_east()
                elif event.key == pygame.K_UP:
                    self.player.try_move_north()
                elif event.key == pygame.K_DOWN:
                    self.player.try_move_south()
                if isinstance(self.player.cell, DownStairs):
                    self.level += 1
                    self.floor = self.dungeon[self.level]
                    for row in self.floor:
                        for cell in row:
                            if isinstance(cell, UpStairs):
                                cell.add(self.player)
                                break
                elif isinstance(self.player.cell, UpStairs) and self.level > 0:
                    self.level -= 1
                    self.floor = self.dungeon[self.level]
                    for row in self.floor:
                        for cell in row:
                            if isinstance(cell, DownStairs):
                                cell.add(self.player)
                                break
            elif event.type == pygame.MOUSEWHEEL:
                self.cell_size += event.y
                if self.cell_size < 1:
                    self.cell_size = 1
                self.font = Font(self.font_file.name, self.cell_size)

    def run(self) -> None:
        # Maybe pygame.SCALED in some circumstances.  It's not too bad on my 4K
        # monitor, but it could be painful on a retina display.
        # Might be better to manually do the same thing to make sure text stays smooth.
        screen = pygame.display.set_mode((800, 600), flags=(pygame.RESIZABLE | pygame.HWSURFACE | pygame.ASYNCBLIT))
        last_frame_time = datetime.min
        # Max of 30 fps
        mintime = 1 / 30
        try:
            while True:
                self.handle_events()

                screen_size = screen.get_size()
                half_width = screen_size[0] // 2
                half_height = screen_size[1] // 2
                half_cell = self.cell_size // 2
                player_position = self.player.position
                player_offset_x = player_position[0] * self.cell_size
                player_offset_y = player_position[1] * self.cell_size

                offset_x = half_width - player_offset_x - half_cell
                offset_y = half_height - player_offset_y - half_cell

                screen.fill(Color(0, 0, 0))
                for y, row in enumerate(self.floor):
                    for x, cell in enumerate(row):
                        screen.blit(
                            cell.render(self.font, self.cell_size),
                            dest=(x * self.cell_size + offset_x, y * self.cell_size + offset_y),
                        )

                # Ensure we don't exceed render time, as otherwise we can eat a lot
                # of CPU rendering and running.
                time_since_last_frame = datetime.utcnow() - last_frame_time
                if time_since_last_frame.total_seconds() < mintime:
                    sleep(mintime - time_since_last_frame.total_seconds())

                pygame.display.flip()

                last_frame_time = datetime.utcnow()
        except (KeyboardInterrupt, Exit):
            pass
        finally:
            pygame.quit()
