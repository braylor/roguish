from roguish.engine import Engine
import locale


def main() -> None:
    locale.setlocale(locale.LC_ALL, '')
    engine = Engine()
    engine.run()


if __name__ == '__main__':
    main()
